No.1 - Mengubah fungsi menjadi fungsi arrow 
// 
const golden = function goldenfunction() => 'this is golden!!';
    console.log(golden());
// 
const golden = () => 'this is golden!!';
    console.log(golden());
// 
No.2 - Sederhanakan menjadi Object literal di ES6 
// 
Object Literal
var orang = {
    firstname : 'wiliam',
    lastname : 'Imoah',
    fullname : function(){
      return + this.firstname + this.lastname;
    }
}
// 
No.3 - Destructuring
// 
let employee = {   
    firstName: "Harry",   
    lastName: "Potter Holt",   
    destination: "Hogwarts React Conf",   
    occupation: "Deve-wizard Avocado" 
};

let {firstName, lastName, destination, occupation} = employee;

console.log(firstName, lastName, destination, occupation);
// 
No.4 - Array Spreading
// 
const mhs = ['akbar', 'galih', 'dewi', 'yusuf'];
const dosen = ['arief', 'guntur', 'dewa', 'hasby'];

const orang = mhs.concat(dosen);

console.log(orang);
// 
No.5 - Template Literals 

const planet = "earth" 
const view = "glass" 
var before =('Lorem' + view + 'dolor sit amet, ' + 
            'consectetur adipiscing elit,' + planet + 'do eiusmod tempor' +
            'incididunt ut labore et dolore magna aliqua. ut enim' +
            'ad minim venim');
    console.log(before);


